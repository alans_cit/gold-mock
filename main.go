package main

import (
	"bytes"
	"c4-mobile-adapter/application/server"
	"c4-mobile-adapter/usecase/model"
	"crypto/aes"
	"crypto/cipher"
	b64 "encoding/base64"
	"encoding/gob"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

const (
	CipherKey     = "AES256Key-32Characters1234567890"
	EncryptSecret = "bb8ef84243d2ee95a41c6c57"
)

// InserirPedidoResult struct
type InserirPedidoResult struct {
	ResultadoOperacao string `json:"ResultadoOperacao"`
	MensagemErro      string `json:"MensagemErro"`
}

// Response struct
type Response struct {
	InserirPedidoResult InserirPedidoResult `json:"InserirPedidoResult"`
}

// Response OrderRequest struct
type OrderRequest struct {
	CartCode                   string
	UserName                   string
	UserCpf                    string
	UserEmail                  string
	UserHybrisId               string
	UserToken                  string
	PaymentInfo                Payment
	PaymentTransactionResponse *PaymentTransactionResponse
	PointOfService             *PointOfServiceResponse
	Cart                       *CartResponse
	logheader                  server.LogHeader
}

type Payment struct {
	Provider     string
	Installments int `validate:"required"`
	Card         Card
}

type
	PointOfServiceResponse struct {
		Name    string                        `json:"name,omitempty"`
		Address PointOfServiceAddressResponse `json:"address,omitempty"`
	}

type CartResponse struct {
	CartCode          string                 `json:"cartCode,omitempty"`
	TotalItems        int64                  `json:"totalItems,omitempty"`
	Total             float64                `json:"total,omitempty"`
	TotalWithDiscount float64                `json:"totalWithDiscount,omitempty"`
	Entries           []CartEntry            `json:"entries,omitempty"`
	Channel           string                 `json:"channel,omitempty"`
	PointOfService    PointOfServiceResponse `json:"pointOfService,omitempty"`
	Cards             []CardEntry            `json:"cards,omitempty"`
	Found             bool                   `json:"-"`
	Valid             bool                   `json:"-"`
	Unauthorized      bool                   `json:"-"`
	AuthAlreadyExists bool                   `json:"paymentAuthorized"`
	Errors            []model.ErrorResponse  `json:"errors,omitempty"`
}

type ProductDetailResponse struct {
	Code              string                `json:"code,omitempty"`
	Price             float64               `json:"price,omitempty"`
	PriceWithDiscount float64               `json:"priceWithDiscount,omitempty"`
	Name              string                `json:"name,omitempty"`
	ImageURL          string                `json:"imageUrl,omitempty"`
	Description       string                `json:"description,omitempty"`
	Brand             string                `json:"brand,omitempty"`
	Ean               string                `json:"ean,omitempty"`
	Found             bool                  `json:"-"`
	Valid             bool                  `json:"-"`
	CalculatedPrice   float64               `json:"calculatedPrice,omitempty"`
	Quantity          float64               `json:"quantity,omitempty"`
	WeightProduct     bool                  `json:"weightProduct,omitempty"`
	Unit              string                `json:"unit,omitempty"`
	Errors            []model.ErrorResponse `json:"errors,omitempty"`
}

type CartEntry struct {
	Product                ProductDetailResponse `json:"product"`
	EntryNumber            int64                 `json:"entryNumber"`
	Quantity               float64               `json:"quantity"`
	TotalPrice             float64               `json:"totalPrice"`
	TotalPriceWithDiscount float64               `json:"totalPriceWithDiscount,omitempty"`
	UnitPrice              float64               `json:"unitPrice"`
	UnitType               string                `json:"unitType"`
	VoucherValue           float64               `json:"voucherValue,omitempty"`
}

type CardEntry struct {
	CardID            string `json:"cardID"`
	AccountHolderName string `json:"accountHolderName"`
	BraspagToken      string `json:"braspagToken"`
	CardNumber        string `json:"cardNumber"`
	CardType          string `json:"cardType"`
	ExpiryMonth       string `json:"expiryMonth"`
	ExpiryYear        string `json:"expiryYear"`
}

type PointOfServiceAddressResponse struct {
	Line1      string                               `json:"line1,omitempty"`
	PostalCode string                               `json:"postalCode,omitempty"`
	Town       string                               `json:"town,omitempty"`
	Country    PointOfServiceAddressCountryResponse `json:"country,omitempty"`
}

type PointOfServiceAddressCountryResponse struct {
	ISOCode string `json:"isocode,omitempty"`
}

type Card struct {
	ID             string
	Token          string
	SecurityCode   string
	Brand          string
	Number         string
	ExpirationDate string
	Holder         string
	TypeCarrefour  bool
}

type PaymentTransactionResponse struct {
	Payments            []PaymentResponse     `json:"payments,omitempty"`
	Errors              []model.ErrorResponse `json:"errors,omitempty"`
	Success             bool                  `json:"-"`
	Unauthorized        bool                  `json:"-"`
	InternalServerError bool                  `json:"-"`
	BadRequest          bool                  `json:"-"`
}

type PaymentResponse struct {
	PaymentID             string       `json:"paymentId"`
	ReceivedDate          time.Time    `json:"receivedDate"`
	Amount                float64      `json:"amount"`
	AcquirerTransactionID string       `json:"acquirerTransactionId"`
	ProofOfSale           string       `json:"proofOfSale"`
	AuthorizationCode     string       `json:"authorizationCode"`
	ReasonCode            string       `json:"reasonCode"`
	ReasonMessage         string       `json:"reasonMessage"`
	Status                string       `json:"status"`
	Installments          int32        `json:"installments"`
	Provider              string       `json:"provider"`
	PaymentType           string       `json:"type"`
	Card                  CardResponse `json:"card"`
}

type CardResponse struct {
	ID             string `json:"id"`
	Token          string `json:"token"`
	Number         string `json:"number"`
	Brand          string `json:"brand"`
	TypeCarrefour  bool   `json:"typeCarrefour"`
	Holder         string `json:"holder"`
	ExpirationDate string `json:"expirationDate"`
}

// Response Order struct
type Order struct {
	DadosPedidos []struct {
		CodigoPedido            string      `json:"CodigoPedido"`
		CodigoEntrega           string      `json:"CodigoEntrega"`
		EMail                   string      `json:"EMail"`
		CPFouCNPJ               string      `json:"CPFouCNPJ"`
		CodigoCliente           string      `json:"CodigoCliente"`
		CondicaoPagamento       string      `json:"CondicaoPagamento"`
		ValorPedido             float64     `json:"ValorPedido"`
		ValorFrete              float64     `json:"ValorFrete"`
		ValorEncargos           float64     `json:"ValorEncargos"`
		ValorDesconto           float64     `json:"ValorDesconto"`
		DataVenda               string      `json:"DataVenda"`
		Transportadora          string      `json:"Transportadora"`
		EmitirNotaSimbolica     bool        `json:"EmitirNotaSimbolica"`
		FormaPagamento          string      `json:"FormaPagamento"`
		DestNome                string      `json:"DestNome"`
		DestSexo                string      `json:"DestSexo"`
		DestEmail               string      `json:"DestEmail"`
		DestTelefone            string      `json:"DestTelefone"`
		DestLogradouro          string      `json:"DestLogradouro"`
		DestNumeroLogradouro    string      `json:"DestNumeroLogradouro"`
		DestComplementoEndereco string      `json:"DestComplementoEndereco"`
		DestBairro              string      `json:"DestBairro"`
		DestMunicipio           string      `json:"DestMunicipio"`
		DestEstado              string      `json:"DestEstado"`
		DestCep                 string      `json:"DestCep"`
		DestPais                string      `json:"DestPais"`
		DestTipoPessoa          string      `json:"DestTipoPessoa"`
		DestInscricaoEstadual   interface{} `json:"DestInscricaoEstadual"`
		DestInscricaoMunicipal  interface{} `json:"DestInscricaoMunicipal"`
		DestReferencia          string      `json:"DestReferencia"`
		PedidoJaPago            bool        `json:"PedidoJaPago"`
		Itens                   struct {
			DadosPedidosItem []struct {
				CodigoProduto      string      `json:"CodigoProduto"`
				QuantidadeProduto  float64     `json:"QuantidadeProduto"`
				PrecoUnitario      float64     `json:"PrecoUnitario"`
				EmbalagemPresente  bool        `json:"EmbalagemPresente"`
				MensagemPresente   interface{} `json:"MensagemPresente"`
				PrecoUnitarioBruto float64     `json:"PrecoUnitarioBruto"`
				ValorReferencia    float64     `json:"ValorReferencia"`
				ValorDesconto      float64     `json:"ValorDesconto"`
				ValorFrete         float64     `json:"ValorFrete"`
				InformAdicionaisNF interface{} `json:"InformAdicionaisNF"`
			} `json:"DadosPedidosItem"`
		} `json:"Itens"`
		DataPrazoEntregaInicial string  `json:"DataPrazoEntregaInicial"`
		DataPrazoEntregaFinal   string  `json:"DataPrazoEntregaFinal"`
		TipoEntrega             string  `json:"TipoEntrega"`
		TipoPedido              string  `json:"TipoPedido"`
		ValorFretePagar         float64 `json:"ValorFretePagar"`
		InformAdicionaisNF      string  `json:"InformAdicionaisNF"`
		PrioridadeEntrega       bool    `json:"PrioridadeEntrega"`
		EndCodigoIBGE           string  `json:"EndCodigoIBGE"`
		WareHouse               string  `json:"WareHouse"`
		QuantidadePacote        int     `json:"QuantidadePacote"`
		EndFaturamento          struct {
			NomeCliente         string `json:"NomeCliente"`
			Logradouro          string `json:"Logradouro"`
			NumeroLogradouro    string `json:"NumeroLogradouro"`
			ComplementoEndereco string `json:"ComplementoEndereco"`
			Bairro              string `json:"Bairro"`
			Municipio           string `json:"Municipio"`
			Estado              string `json:"Estado"`
			Cep                 string `json:"Cep"`
			ReferenciaEndereco  string `json:"ReferenciaEndereco"`
			Pais                string `json:"Pais"`
			EndCodigoIBGE       string `json:"EndCodigoIBGE"`
		} `json:"EndFaturamento"`
	} `json:"DadosPedidos"`
}

type UpdateOrderRequest struct {
	OrderCode           string
	UserCpf             string
	UserEmail           string
	UserToken           string
	PaymentTransactions []UpdatePaymentTransactionRequest `json:"paymentTransactions,omitempty"`
	logheader           server.LogHeader
}

type UpdatePaymentTransactionRequest struct {
	CurrencyIsoCode string               `json:"currencyIsocode,omitempty"`
	Installments    int32                `json:"installments,omitempty"`
	PaymentProvider string               `json:"paymentProvider,omitempty"`
	PlannedAmount   float64              `json:"plannedAmount,omitempty"`
	Type            string               `json:"type,omitempty"`
	Entries         []UpdatePaymentEntry `json:"entries,omitempty"`
	PaymentInfo     PaymentInfo          `json:"paymentInfo,omitempty"`
}

type UpdatePaymentEntry struct {
	CurrencyIsoCode        string  `json:"currencyIsocode,omitempty"`
	Amount                 float64 `json:"amount,omitempty"`
	CPF                    string  `json:"cpf,omitempty"`
	AcquirerTransactionID  string  `json:"acquirerTransactionId,omitempty"`
	GatewayOrderID         string  `json:"gatewayOrderId,omitempty"`
	GatewayTransactionID   string  `json:"gatewayTransactionId,omitempty"`
	AuthorizationCode      string  `json:"authorizationCode,omitempty"`
	ProofOfSale            string  `json:"proofOfSale,omitempty"`
	PaymentDate            string  `json:"paymentDate,omitempty"`
	ReasonCode             string  `json:"reasonCode,omitempty"`
	ReasonMessage          string  `json:"reasonMessage,omitempty"`
	Type                   string  `json:"type,omitempty"`
	GatewayPaymentProvider string  `json:"gatewayPaymentProvider,omitempty"`
	GatewayPaymentType     string  `json:"gatewayPaymentType,omitempty"`
}

type PaymentInfo struct {
	ID                string      `json:"id,omitempty"`
	Code              string      `json:"code,omitempty"`
	AccountHolderName string      `json:"accountHolderName,omitempty"`
	CardNumber        string      `json:"cardNumber,omitempty"`
	CardType          CardType    `json:"cardType,omitempty"`
	ExpiryMonth       string      `json:"expiryMonth,omitempty"`
	ExpiryYear        string      `json:"expiryYear,omitempty"`
	Token             string      `json:"token,omitempty"`
	Saved             bool        `json:"saved,omitempty"`
	BillingAddress    AddressData `json:"billingAddress,omitempty"`
}

type CardType struct {
	Code string `json:"code,omitempty"`
}

type AddressData struct {
	Line1      string `json:"line1,omitempty"`
	Town       string `json:"town,omitempty"`
	PostalCode string `json:"postalCode,omitempty"`
	Country    struct {
		ISOCode string `json:"isocode,omitempty"`
	} `json:"country,omitempty"`
}

func main() {
	fmt.Println("Server initializing.")

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", Index)
	router.HandleFunc("/InserirPedido", OrderWS)
	router.HandleFunc("/DecriptUpdateOrder", DecriptUpdateOrderWS)
	router.HandleFunc("/DecriptPlaceOrder", DecriptPlaceOrderWS)

	// These two lines are important if you're designing a front-end to utilise this API methods
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT"})

	fmt.Println("initialized....")
	// Launch server with CORS validations
	fmt.Println(http.ListenAndServe(":4034", handlers.CORS(allowedOrigins, allowedMethods)(router)))
}

// Index -> Index Router
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Welcome!")
}

// OrderWS -> WS Pedido Router
func OrderWS(w http.ResponseWriter, r *http.Request) {
	dump, err := httputil.DumpRequest(r, true)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%q", dump)

	var order Order
	_ = json.NewDecoder(r.Body).Decode(&order)

	log.Println("**** INI ****")
	spew.Dump(order)
	log.Println("**** END ****")

	result := Response{
		InserirPedidoResult: InserirPedidoResult{
			ResultadoOperacao: "1",
			MensagemErro:      "OK",
		},
	}

	js, err := json.Marshal(result)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

// formatRequest generates ascii representation of a request
func formatRequest(r *http.Request) string {
	// Create return string
	var request []string
	// Add the request string
	url := fmt.Sprintf("%v %v %v", r.Method, r.URL, r.Proto)
	request = append(request, url)
	// Add the host
	request = append(request, fmt.Sprintf("Host: %v", r.Host))
	// Loop through headers
	for name, headers := range r.Header {
		name = strings.ToLower(name)
		for _, h := range headers {
			request = append(request, fmt.Sprintf("%v: %v", name, h))
		}
	}

	// If this is a POST, add post data
	if r.Method == "POST" {
		r.ParseForm()
		request = append(request, "\n")
		request = append(request, r.Form.Encode())
	}
	// Return the request as a string
	return strings.Join(request, "\n")
}

// DecriptPlaceOrderWS -> WS DecriptPlaceOrder Router
func DecriptPlaceOrderWS(w http.ResponseWriter, r *http.Request) {
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}

	var decoded, _ = b64.StdEncoding.DecodeString(string(bodyBytes))
	var decrypted = decrypt(decoded)

	log.Println("**** INI ****")
	log.Println(decrypted)
	log.Println("**** END ****")

	w.Header().Set("Content-Type", "application/json")

	buffer := bytes.NewBuffer(decrypted)
	var order OrderRequest
	decoder := gob.NewDecoder(buffer)
	e := decoder.Decode(&order)

	if e != nil {
		log.Println("Error converting byte to OrderRequest ", e)
	}

	log.Println(order)

	b, err := json.Marshal(order)
	if err != nil {
		log.Println("Error converting OrderRequest to json", e)
	}

	w.Write(b)
}

// DecriptUpdateOrderWS -> WS DecriptUpdateOrder Router
func DecriptUpdateOrderWS(w http.ResponseWriter, r *http.Request) {
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}

	var decoded, _ = b64.StdEncoding.DecodeString(string(bodyBytes))
	var decrypted = decrypt(decoded)

	log.Println("**** INI ****")
	log.Println(decrypted)
	log.Println("**** END ****")

	w.Header().Set("Content-Type", "application/json")

	buffer := bytes.NewBuffer(decrypted)
	var order UpdateOrderRequest
	decoder := gob.NewDecoder(buffer)
	e := decoder.Decode(&order)

	if e != nil {
		log.Println("Error converting byte to UpdateOrderRequest ", e)
	}

	log.Println(order)

	b, err := json.Marshal(order)
	if err != nil {
		log.Println("Error converting UpdateOrderRequest to json", e)
	}

	w.Write(b)
}

func decrypt(content []byte) []byte {
	contentAsString := fmt.Sprintf("%x", content)
	ciphered, _ := hex.DecodeString(contentAsString)

	nonce, err := hex.DecodeString(EncryptSecret)
	if err != nil {
		log.Println(err.Error())
	}

	block, err := aes.NewCipher([]byte(CipherKey))
	if err != nil {
		log.Println(err.Error())
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		log.Println(err.Error())
	}

	decryptedText, err := aesgcm.Open(nil, nonce, ciphered, nil)
	if err != nil {
		log.Println(err.Error())
	}

	return decryptedText
}
