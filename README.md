import packages 
go get github.com/gorilla/mux
go get github.com/gorilla/handlers
go get github.com/davecgh/go-spew/spew

build and exec > go build & ./mock-ws
only exec > go run main.go 


Endpoints

InserirPedido
POST {{host}}:{{port}}/InserirPedido
Request Body
Response Body

DecriptPlaceOrder
POST {{host}}:{{port}}/DecriptPlaceOrder
Request Body >> Conte�do da request da place.order.DLQ (Base64)
Response Body >> PlaceOrderRequest

DecriptUpdateOrder
POST {{host}}:{{port}}/DecriptUpdateOrder
Request Body >> Conte�do da request da update.order.DLQ (Base64)
Response Body >> UpdateOrderRequest